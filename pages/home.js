import { useCallback } from "react";
import { useRouter } from "next/router";
import styles from "./home.module.css";

const Home = () => {
  const router = useRouter();

  const onButtonClick = useCallback(() => {
    router.push("/collaborations");
  }, [router]);

  const onButton1Click = useCallback(() => {
    router.push("/collaborations");
  }, [router]);

  return (
    <div className={styles.homeDiv}>
      <section className={styles.rectangleSection} />
      <section className={styles.frameSection}>
        <div className={styles.headerDiv}>
          <div className={styles.contentDiv}>
            <div className={styles.textDiv}>
              <h6 className={styles.headerH6}>Checkmate</h6>
              <p className={styles.subheadP}>Gaming Cafe</p>
            </div>
          </div>
        </div>
        <img className={styles.mediaIcon} alt="" src="../media@2x.png" />
        <div className={styles.supportingTextDiv}>
          <p className={styles.supportingText}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor
          </p>
        </div>
        <button className={styles.button} onClick={onButtonClick}>
          <div className={styles.labelTextDiv}>See Collaboration</div>
        </button>
        <div className={styles.headerDiv1}>
          <div className={styles.contentDiv}>
            <div className={styles.textDiv}>
              <h6 className={styles.headerH61}>AlphaCostumes</h6>
              <p className={styles.subheadP}>
                Professional Cosplay Costume Maker
              </p>
            </div>
          </div>
        </div>
        <img className={styles.mediaIcon1} alt="" src="../media1@2x.png" />
        <p className={styles.supportingText1}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
        <b className={styles.b}>🤝</b>
      </section>
      <h2 className={styles.weConnectRetailEntrepeneurs}>
        <span>{`We connect `}</span>
        <span className={styles.retailEntrepeneursSpan}>
          retail entrepeneurs
        </span>
        <span>{` with `}</span>
        <span className={styles.placesSpan}>places</span>
        <span className={styles.retailEntrepeneursSpan}>{` `}</span>
        <span>to sell their product.</span>
      </h2>
      <div className={styles.makersHobbyistsDiv}>
        <p className={styles.makersP}>makers</p>
        <p className={styles.hobbyistsP}>hobbyists</p>
      </div>
      <div className={styles.cafesVenuesDiv}>
        <p className={styles.makersP}>cafes</p>
        <p className={styles.hobbyistsP}>venues</p>
      </div>
      <div className={styles.artisansCreativesDiv}>
        <p className={styles.makersP}>artisans</p>
        <p className={styles.hobbyistsP}>creatives</p>
      </div>
      <div className={styles.lobbiesEventsDiv}>
        <p className={styles.makersP}>lobbies</p>
        <p className={styles.hobbyistsP}>events</p>
      </div>
      <button className={styles.button1} autoFocus onClick={onButton1Click}>
        <b className={styles.labelText}>Continue to Main Site</b>
      </button>
    </div>
  );
};

export default Home;
