import { useState, useCallback } from "react";
import CollaborationsPopup1 from "../components/collaborations-popup1";
import PortalPopup from "../components/portal-popup";
import DrawerColl from "../components/drawer-coll";
import PortalDrawer from "../components/portal-drawer";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import styles from "./collaborations.module.css";

const Collaborations = () => {
  const [isCollaborationsPopupOpen, setCollaborationsPopupOpen] =
    useState(false);
  const [isDrawerCollOpen, setDrawerCollOpen] = useState(false);
  const [isDrawerColl1Open, setDrawerColl1Open] = useState(false);
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);
  const [isCollaborationsPopup1Open, setCollaborationsPopup1Open] =
    useState(false);

  const openCollaborationsPopup = useCallback(() => {
    setCollaborationsPopupOpen(true);
  }, []);

  const closeCollaborationsPopup = useCallback(() => {
    setCollaborationsPopupOpen(false);
  }, []);

  const openDrawerColl = useCallback(() => {
    setDrawerCollOpen(true);
  }, []);

  const closeDrawerColl = useCallback(() => {
    setDrawerCollOpen(false);
  }, []);

  const openDrawerColl1 = useCallback(() => {
    setDrawerColl1Open(true);
  }, []);

  const closeDrawerColl1 = useCallback(() => {
    setDrawerColl1Open(false);
  }, []);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const openCollaborationsPopup1 = useCallback(() => {
    setCollaborationsPopup1Open(true);
  }, []);

  const closeCollaborationsPopup1 = useCallback(() => {
    setCollaborationsPopup1Open(false);
  }, []);

  return (
    <>
      <div
        className={styles.collaborationsDiv}
        onClick={openCollaborationsPopup1}
      >
        <iframe
          className={styles.rectangleIframe}
          src={`https://maps.google.com/maps?q=charring%20cross&t=&z=15&ie=UTF8&iwloc=&output=embed`}
          onClick={openCollaborationsPopup}
        />
        <img
          className={styles.locationOnIcon}
          alt=""
          src="../location-on.svg"
        />
        <button className={styles.iconButton} onClick={openDrawerColl}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios.svg"
          />
        </button>
        <img className={styles.zoomOutIcon} alt="" src="../zoom-out.svg" />
        <img className={styles.zoomInIcon} alt="" src="../zoom-in.svg" />
        <button className={styles.mapViewButton} onClick={openDrawerColl1}>
          <div className={styles.labelTextDiv}>Map View: Collaborations</div>
        </button>
        <div className={styles.headerMainsiteDiv}>
          <Link href="/">
            <a className={styles.shareAshelf}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv1}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA}>Sign Up:</a>
          </Link>
        </div>
        <div className={styles.formFooterDiv}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv} />
        </div>
      </div>
      {isCollaborationsPopupOpen && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeCollaborationsPopup}
        >
          <CollaborationsPopup1 onClose={closeCollaborationsPopup} />
        </PortalPopup>
      )}
      {isDrawerCollOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Left"
          onOutsideClick={closeDrawerColl}
        >
          <DrawerColl onClose={closeDrawerColl} />
        </PortalDrawer>
      )}
      {isDrawerColl1Open && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Left"
          onOutsideClick={closeDrawerColl1}
        >
          <DrawerColl onClose={closeDrawerColl1} />
        </PortalDrawer>
      )}
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
      {isCollaborationsPopup1Open && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeCollaborationsPopup1}
        >
          <CollaborationsPopup1 onClose={closeCollaborationsPopup1} />
        </PortalPopup>
      )}
    </>
  );
};

export default Collaborations;
