import React, { useEffect, useCallback} from "react";
import { useRouter } from "next/router";
import styles from "./index.module.css";

const Start = () => {
  const router = useRouter();

  const onRectangleRectangleClick = useCallback(() => {
    router.push("/");
  }, [router]);

  return (
    useEffect(() => {
      window.setTimeout(function() {
        window.location.href = '/home';
    }, 1000)
  }),
    <div className={styles.startDiv}>
      <div
        className={styles.rectangleDiv}
        onClick={onRectangleRectangleClick}
      />
      <div className={styles.shareourshelfH1}>
        <h1 className={styles.shareourshelfH11}>
          <span className={styles.shareourshelfTxtSpan}>
            <span className={styles.shareSpan}>share</span>
            <span className={styles.aSpan}>A</span>
            <span className={styles.shelfSpan}>shelf</span>
          </span>
        </h1>
      </div>
      <h5 className={styles.theCollaborativeMarketplace}>
        The Collaborative Marketplace
      </h5>
    </div>
  );
};

export default Start;
