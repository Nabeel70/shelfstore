import { useState, useCallback } from "react";
import {FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import { useRouter } from "next/router";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./signup.module.css";

const Signup = () => {
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);
   const router = useRouter();
  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);
  
   const onSignupButtonClick = useCallback(() => {
    router.push("/collaborations");
  }, [router]);

  return (
    <>
      <div className={styles.signupDiv}>
        <div className={styles.formHeaderDiv}>
          <div className={styles.lineDiv} />
          <Link href="/">
            <a className={styles.shareAshelf}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <button className={styles.groupButton1}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </button>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
        </div>
        <div className={styles.formFooterDiv}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv1} />
        </div>
        <h2 className={styles.letsGetYouConnected}>Let’s Get You Connected!</h2>
        <h6 className={styles.trailingDataH6}>Type of account:</h6>


       
        <form className={styles.signupForm} method="post" action= "/collaborations">

<FormControl>
  <RadioGroup aria-labelledby="demo-radio-buttons-group-label" name="radio-buttons-group" >
    <FormControlLabel  value="Host"   label="Host" control={<Radio color="warning" size="medium" />} />
    <FormControlLabel value="Vendor"  label="Vendor" control={<Radio color="warning" size="medium" />} />
  </RadioGroup>
</FormControl>
        
        <div className={styles.emailDiv} >Email:</div>
        <input className={styles.emailInput} type="email" autoComplete="on" required />
        <div className={styles.usernameDiv}>Username:</div>
        <input className={styles.rectangleInput2} type="text" autoComplete="on" required />
          <div className={styles.passwordDiv}>Password:</div>
          <input className={styles.rectangleInput} type="password" required />
          <div className={styles.retypePasswordDiv}>Retype Password:</div>
          <input className={styles.rectangleInput1} type="password" required />

          <button
            className={styles.signupButton}
            autoFocus
          >
            <b className={styles.labelText}>Signup</b>
          </button>
        </form>


        <h1 className={styles.theCollaborativeMarketplace}>
          The Collaborative Marketplace
        </h1>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default Signup;

