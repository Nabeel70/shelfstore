exports.id = 338;
exports.ids = [338];
exports.modules = {

/***/ 2737:
/***/ ((module) => {

// Exports
module.exports = {
	"rectangleDiv": "menu-drawer_rectangleDiv__s6WvF",
	"hostsA": "menu-drawer_hostsA__PVsFE",
	"main": "menu-drawer_main__PwL1g",
	"signupA": "menu-drawer_signupA__aJ0FP",
	"frameDiv": "menu-drawer_frameDiv__7Mj0W",
	"homeA": "menu-drawer_homeA__PZw_M",
	"menuDrawerDiv": "menu-drawer_menuDrawerDiv__goiz4",
	"animate": "menu-drawer_animate__qUFQF",
	"slide-in-right": "menu-drawer_slide-in-right__CYP4v"
};


/***/ }),

/***/ 1795:
/***/ ((module) => {

// Exports
module.exports = {
	"portalPopupOverlay": "portal-drawer_portalPopupOverlay__XG1fv"
};


/***/ }),

/***/ 4627:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9894);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2737);
/* harmony import */ var _menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3__);




const MenuDrawer = ({ onClose  })=>{
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        const scrollAnimElements = document.querySelectorAll("[data-animate-on-scroll]");
        const observer = new IntersectionObserver((entries)=>{
            for (const entry of entries){
                if (entry.isIntersecting || entry.intersectionRatio > 0) {
                    const targetElement = entry.target;
                    targetElement.classList.add((_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().animate));
                    observer.unobserve(targetElement);
                }
            }
        }, {
            threshold: 0.15
        });
        for(let i = 0; i < scrollAnimElements.length; i++){
            observer.observe(scrollAnimElements[i]);
        }
        return ()=>{
            for(let i = 0; i < scrollAnimElements.length; i++){
                observer.unobserve(scrollAnimElements[i]);
            }
        };
    }, []);
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().menuDrawerDiv),
        "data-animate-on-scroll": true,
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().frameDiv),
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().frameDiv),
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().rectangleDiv)
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_2___default()), {
                            href: "/signup",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().signupA),
                                children: "Signup"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_2___default()), {
                            href: "/collaborations",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().main),
                                children: "Main"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_2___default()), {
                            href: "/hosts",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().hostsA),
                                children: "Hosts"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_2___default()), {
                href: "/",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                    className: (_menu_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().homeA),
                    children: "Home"
                })
            })
        ]
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MenuDrawer);


/***/ }),

/***/ 4859:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony export DrawerContainer */
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6405);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _portal_drawer_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1795);
/* harmony import */ var _portal_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_portal_drawer_module_css__WEBPACK_IMPORTED_MODULE_3__);




const PortalDrawer = ({ children , overlayColor , placement ="Left" , onOutsideClick , zIndex =100 ,  })=>{
    const drawerStyle = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{
        const style = {};
        style.zIndex = zIndex;
        if (overlayColor) {
            style.backgroundColor = overlayColor;
        }
        switch(placement){
            case "Left":
                style.alignItems = "flex-start";
                break;
            case "Right":
                style.alignItems = "flex-end";
                break;
            case "Top":
                style.alignItems = "center";
                break;
            case "Bottom":
                style.alignItems = "center";
                style.justifyContent = "flex-end";
                break;
        }
        return style;
    }, [
        placement,
        overlayColor,
        zIndex
    ]);
    const onOverlayClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((e)=>{
        if (onOutsideClick && e.target.classList.contains((_portal_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().portalPopupOverlay))) {
            onOutsideClick();
        }
        e.stopPropagation();
    }, [
        onOutsideClick
    ]);
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DrawerContainer, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_portal_drawer_module_css__WEBPACK_IMPORTED_MODULE_3___default().portalPopupOverlay),
            style: drawerStyle,
            onClick: onOverlayClick,
            children: children
        })
    });
};
const DrawerContainer = ({ children , containerId ="portals"  })=>{
    if (false) {} else {
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {});
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PortalDrawer);


/***/ })

};
;