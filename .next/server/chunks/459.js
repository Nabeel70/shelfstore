exports.id = 459;
exports.ids = [459];
exports.modules = {

/***/ 2070:
/***/ ((module) => {

// Exports
module.exports = {
	"headerH6": "collaborations-popup1_headerH6__gCt6Q",
	"subheadP": "collaborations-popup1_subheadP__v4BFx",
	"textDiv": "collaborations-popup1_textDiv__PWiKP",
	"closeButton": "collaborations-popup1_closeButton__G5IDy",
	"contentDiv": "collaborations-popup1_contentDiv__VRByX",
	"headerDiv": "collaborations-popup1_headerDiv__LgZsO",
	"mediaIcon": "collaborations-popup1_mediaIcon__gwAay",
	"supportingText": "collaborations-popup1_supportingText__vZb9Z",
	"supportingTextDiv": "collaborations-popup1_supportingTextDiv__hW_Ym",
	"labelTextDiv": "collaborations-popup1_labelTextDiv__5eRKo",
	"buttonDiv": "collaborations-popup1_buttonDiv__xM9dw",
	"labelTextDiv1": "collaborations-popup1_labelTextDiv1__jph9G",
	"button": "collaborations-popup1_button__AwlFa",
	"headerH61": "collaborations-popup1_headerH61__aIawx",
	"headerDiv1": "collaborations-popup1_headerDiv1__iAEVL",
	"mediaIcon1": "collaborations-popup1_mediaIcon1__rYjr1",
	"button1": "collaborations-popup1_button1__tn4vm",
	"buttonDiv1": "collaborations-popup1_buttonDiv1__NSQm9",
	"supportingTextDiv1": "collaborations-popup1_supportingTextDiv1__iyQ83",
	"augustP": "collaborations-popup1_augustP__MpiDp",
	"b": "collaborations-popup1_b__vd7Ry",
	"collaborationsPopupDiv": "collaborations-popup1_collaborationsPopupDiv__xBOCs"
};


/***/ }),

/***/ 7571:
/***/ ((module) => {

// Exports
module.exports = {
	"contactDiv": "drawer-coll_contactDiv__EyP_c",
	"shareashelfLtdDiv": "drawer-coll_shareashelfLtdDiv__rjR1i",
	"supportDiv": "drawer-coll_supportDiv__oHVxy",
	"termsOfService": "drawer-coll_termsOfService__YNHJY",
	"fieldDiv": "drawer-coll_fieldDiv__89Ya8",
	"frameDropdown": "drawer-coll_frameDropdown__PASy0",
	"mobileMenuDiv": "drawer-coll_mobileMenuDiv__LUMrx",
	"shareSpan": "drawer-coll_shareSpan___Swso",
	"aSpan": "drawer-coll_aSpan__LGj64",
	"shelfSpan": "drawer-coll_shelfSpan___HIME",
	"shareAshelfTxtSpan": "drawer-coll_shareAshelfTxtSpan__aDTYj",
	"shareAshelfDiv": "drawer-coll_shareAshelfDiv__hS1Gg",
	"rectangleDiv": "drawer-coll_rectangleDiv__4A6_l",
	"label": "drawer-coll_label__HwYA8",
	"floatingLabelDiv": "drawer-coll_floatingLabelDiv__2TR3p",
	"trailingDataP": "drawer-coll_trailingDataP__1z_5U",
	"trailingIconFormCheck": "drawer-coll_trailingIconFormCheck__u3br6",
	"trailingDataP1": "drawer-coll_trailingDataP1__wy1Kv",
	"trailingIconFormCheck1": "drawer-coll_trailingIconFormCheck1__rUgkb",
	"trailingDataP2": "drawer-coll_trailingDataP2__ASr_o",
	"trailingIconFormCheck2": "drawer-coll_trailingIconFormCheck2__Is9Gf",
	"trailingDataP3": "drawer-coll_trailingDataP3__k84eE",
	"trailingDataP4": "drawer-coll_trailingDataP4__ZZTzW",
	"trailingIconFormCheck3": "drawer-coll_trailingIconFormCheck3__0lNR9",
	"trailingIconFormCheck4": "drawer-coll_trailingIconFormCheck4__yduNd",
	"trailingDataP5": "drawer-coll_trailingDataP5__CgO9w",
	"trailingIconFormCheck5": "drawer-coll_trailingIconFormCheck5__XGfFi",
	"productCategoriesH6": "drawer-coll_productCategoriesH6__b_LuA",
	"augustStrong": "drawer-coll_augustStrong__MJA_5",
	"collaborationDatesH6": "drawer-coll_collaborationDatesH6__6Ugwe",
	"lineDiv": "drawer-coll_lineDiv__7rhkw",
	"trailingDataP6": "drawer-coll_trailingDataP6__yiBTS",
	"trailingIconFormCheck6": "drawer-coll_trailingIconFormCheck6__rJlje",
	"trailingDataP7": "drawer-coll_trailingDataP7__m7TF7",
	"trailingIconFormCheck7": "drawer-coll_trailingIconFormCheck7__vdyPv",
	"trailingDataP8": "drawer-coll_trailingDataP8__RRp6m",
	"trailingIconFormCheck8": "drawer-coll_trailingIconFormCheck8__PQbhY",
	"trailingDataP10": "drawer-coll_trailingDataP10__dAaqS",
	"trailingDataP9": "drawer-coll_trailingDataP9__D3RrX",
	"trailingIconFormCheck10": "drawer-coll_trailingIconFormCheck10__wM4jy",
	"trailingIconFormCheck9": "drawer-coll_trailingIconFormCheck9__892Qm",
	"trailingDataP11": "drawer-coll_trailingDataP11__PcvNN",
	"trailingIconFormCheck11": "drawer-coll_trailingIconFormCheck11__CuyhR",
	"hostCategoriesH6": "drawer-coll_hostCategoriesH6__5ddpv",
	"lineDiv1": "drawer-coll_lineDiv1__19_ms",
	"frameDiv": "drawer-coll_frameDiv__M9pin",
	"menuPackDiv": "drawer-coll_menuPackDiv__P9I57",
	"drawerCollDiv": "drawer-coll_drawerCollDiv__F_knj",
	"animate": "drawer-coll_animate__4FT2k",
	"slide-in-left": "drawer-coll_slide-in-left__QciCD",
	"drawer-coll_fieldDiv__89Ya8": "drawer-coll_drawer-coll_fieldDiv__89Ya8__NPoYL",
	"drawer-coll_frameDropdown__PASy0": "drawer-coll_drawer-coll_frameDropdown__PASy0__Xu80z"
};


/***/ }),

/***/ 4003:
/***/ ((module) => {

// Exports
module.exports = {
	"portalPopupOverlay": "portal-popup_portalPopupOverlay__u40uf"
};


/***/ }),

/***/ 4602:
/***/ ((module) => {

// Exports
module.exports = {
	"rectangleIframe": "collaborations_rectangleIframe__2HDdx",
	"locationOnIcon": "collaborations_locationOnIcon__aS2BY",
	"arrowForwardIosIcon": "collaborations_arrowForwardIosIcon__RRNTY",
	"iconButton": "collaborations_iconButton__NTY31",
	"zoomInIcon": "collaborations_zoomInIcon__Hrt_C",
	"zoomOutIcon": "collaborations_zoomOutIcon__oPfPu",
	"labelTextDiv": "collaborations_labelTextDiv__QMTx5",
	"mapViewButton": "collaborations_mapViewButton__jbbyK",
	"shareSpan": "collaborations_shareSpan__2vJaX",
	"aSpan": "collaborations_aSpan__9meyv",
	"shelfSpan": "collaborations_shelfSpan__rkiQ1",
	"shareAshelfTxtSpan": "collaborations_shareAshelfTxtSpan__G_BbL",
	"shareAshelf": "collaborations_shareAshelf__0JtdR",
	"userImagesUserImages": "collaborations_userImagesUserImages__JvTlj",
	"labelTextDiv1": "collaborations_labelTextDiv1__4CikT",
	"inputChipDiv": "collaborations_inputChipDiv__gWLUb",
	"menuIcon": "collaborations_menuIcon__4uV3q",
	"menuIcon1": "collaborations_menuIcon1___xUKm",
	"groupDiv": "collaborations_groupDiv__3NLn4",
	"groupButton": "collaborations_groupButton__5F_M8",
	"signUpA": "collaborations_signUpA__7stmA",
	"whatIsShareAshelf": "collaborations_whatIsShareAshelf__xKT0x",
	"headerMainsiteDiv": "collaborations_headerMainsiteDiv__Ul4RN",
	"rectangleDiv": "collaborations_rectangleDiv__Z0Fs1",
	"contactDiv": "collaborations_contactDiv__n1UxK",
	"rentmyshelfIncDiv": "collaborations_rentmyshelfIncDiv___FPaq",
	"supportDiv": "collaborations_supportDiv__stJg0",
	"termsOfService": "collaborations_termsOfService__U0RoX",
	"lineDiv": "collaborations_lineDiv__GMwqn",
	"formFooterDiv": "collaborations_formFooterDiv__5Rhs8",
	"collaborationsDiv": "collaborations_collaborationsDiv__Ko6th"
};


/***/ }),

/***/ 9315:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony export Portal */
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6405);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _portal_popup_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4003);
/* harmony import */ var _portal_popup_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_portal_popup_module_css__WEBPACK_IMPORTED_MODULE_3__);




const PortalPopup = ({ children , overlayColor , placement ="Centered" , onOutsideClick , zIndex =100 ,  })=>{
    const popupStyle = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{
        const style = {};
        style.zIndex = zIndex;
        if (overlayColor) {
            style.backgroundColor = overlayColor;
        }
        switch(placement){
            case "Centered":
                style.alignItems = "center";
                style.justifyContent = "center";
                break;
            case "Top left":
                style.alignItems = "flex-start";
                break;
            case "Top center":
                style.alignItems = "center";
                break;
            case "Top right":
                style.alignItems = "flex-end";
                break;
            case "Bottom left":
                style.alignItems = "flex-start";
                style.justifyContent = "flex-end";
                break;
            case "Bottom center":
                style.alignItems = "center";
                style.justifyContent = "flex-end";
                break;
            case "Bottom right":
                style.alignItems = "flex-end";
                style.justifyContent = "flex-end";
                break;
        }
        return style;
    }, [
        placement,
        overlayColor,
        zIndex
    ]);
    const onOverlayClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((e)=>{
        if (onOutsideClick && e.target.classList.contains((_portal_popup_module_css__WEBPACK_IMPORTED_MODULE_3___default().portalPopupOverlay))) {
            onOutsideClick();
        }
        e.stopPropagation();
    }, [
        onOutsideClick
    ]);
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Portal, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_portal_popup_module_css__WEBPACK_IMPORTED_MODULE_3___default().portalPopupOverlay),
            style: popupStyle,
            onClick: onOverlayClick,
            children: children
        })
    });
};
const Portal = ({ children , containerId ="portals"  })=>{
    if (false) {} else {
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {});
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PortalPopup);


/***/ }),

/***/ 4459:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ collaborations)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-bootstrap"
var external_react_bootstrap_ = __webpack_require__(358);
// EXTERNAL MODULE: ./components/collaborations-popup1.module.css
var collaborations_popup1_module = __webpack_require__(2070);
var collaborations_popup1_module_default = /*#__PURE__*/__webpack_require__.n(collaborations_popup1_module);
;// CONCATENATED MODULE: ./components/collaborations-popup1.js





const CollaborationsPopup1 = ({ onClose  })=>{
    const onButtonClick = (0,external_react_.useCallback)(()=>{
        window.location.href = "#";
    }, []);
    const onButton1Click = (0,external_react_.useCallback)(()=>{
        window.location.href = "#";
    }, []);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: (collaborations_popup1_module_default()).collaborationsPopupDiv,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (collaborations_popup1_module_default()).headerDiv,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (collaborations_popup1_module_default()).contentDiv,
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (collaborations_popup1_module_default()).textDiv,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    className: (collaborations_popup1_module_default()).headerH6,
                                    children: "Checkmate"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: (collaborations_popup1_module_default()).subheadP,
                                    children: "Gaming Cafe"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Button, {
                        className: (collaborations_popup1_module_default()).closeButton,
                        variant: "outline-secondary",
                        href: "/collaborations",
                        children: "✕"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                className: (collaborations_popup1_module_default()).mediaIcon,
                alt: "",
                src: "../media@2x.png"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (collaborations_popup1_module_default()).supportingTextDiv,
                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                    className: (collaborations_popup1_module_default()).supportingText,
                    children: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (collaborations_popup1_module_default()).buttonDiv,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (collaborations_popup1_module_default()).labelTextDiv,
                    children: "Enabled"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                className: (collaborations_popup1_module_default()).button,
                autoFocus: true,
                onClick: onButtonClick,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (collaborations_popup1_module_default()).labelTextDiv1,
                    children: "See Profile"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (collaborations_popup1_module_default()).headerDiv1,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (collaborations_popup1_module_default()).contentDiv,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (collaborations_popup1_module_default()).textDiv,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                className: (collaborations_popup1_module_default()).headerH61,
                                children: "AlphaCostumes"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (collaborations_popup1_module_default()).subheadP,
                                children: "Professional Cosplay Costume Maker"
                            })
                        ]
                    })
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                className: (collaborations_popup1_module_default()).mediaIcon1,
                alt: "",
                src: "../media1@2x.png"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (collaborations_popup1_module_default()).supportingTextDiv1,
                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                    className: (collaborations_popup1_module_default()).supportingText,
                    children: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (collaborations_popup1_module_default()).buttonDiv1,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (collaborations_popup1_module_default()).labelTextDiv,
                    children: "Enabled"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                className: (collaborations_popup1_module_default()).button1,
                autoFocus: true,
                onClick: onButton1Click,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (collaborations_popup1_module_default()).labelTextDiv1,
                    children: "See Profile"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("b", {
                className: (collaborations_popup1_module_default()).b,
                children: "\uD83E\uDD1D"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                className: (collaborations_popup1_module_default()).augustP,
                children: "12-19 August"
            })
        ]
    });
};
/* harmony default export */ const collaborations_popup1 = (CollaborationsPopup1);

// EXTERNAL MODULE: ./components/portal-popup.js
var portal_popup = __webpack_require__(9315);
// EXTERNAL MODULE: ../node_modules/antd/dist/antd.css
var antd = __webpack_require__(6642);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__(7066);
// EXTERNAL MODULE: ./components/drawer-coll.module.css
var drawer_coll_module = __webpack_require__(7571);
var drawer_coll_module_default = /*#__PURE__*/__webpack_require__.n(drawer_coll_module);
;// CONCATENATED MODULE: ./components/drawer-coll.js








const DrawerColl = ({ onClose  })=>{
    (0,external_react_.useEffect)(()=>{
        const scrollAnimElements = document.querySelectorAll("[data-animate-on-scroll]");
        const observer = new IntersectionObserver((entries)=>{
            for (const entry of entries){
                if (entry.isIntersecting || entry.intersectionRatio > 0) {
                    const targetElement = entry.target;
                    targetElement.classList.add((drawer_coll_module_default()).animate);
                    observer.unobserve(targetElement);
                }
            }
        }, {
            threshold: 0.15
        });
        for(let i = 0; i < scrollAnimElements.length; i++){
            observer.observe(scrollAnimElements[i]);
        }
        return ()=>{
            for(let i = 0; i < scrollAnimElements.length; i++){
                observer.unobserve(scrollAnimElements[i]);
            }
        };
    }, []);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: (drawer_coll_module_default()).drawerCollDiv,
        "data-animate-on-scroll": true,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (drawer_coll_module_default()).supportDiv,
                children: "Support"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (drawer_coll_module_default()).contactDiv,
                children: "Contact"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (drawer_coll_module_default()).termsOfService,
                children: "Terms of Service"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (drawer_coll_module_default()).shareashelfLtdDiv,
                children: "\xa9 2022 Shareashelf Ltd."
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (drawer_coll_module_default()).frameDiv,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (drawer_coll_module_default()).mobileMenuDiv,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (drawer_coll_module_default()).fieldDiv,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_antd_.Dropdown, {
                                className: (drawer_coll_module_default()).frameDropdown,
                                overlay: /*#__PURE__*/ jsx_runtime_.jsx(external_antd_.Menu, {
                                    children: [
                                        {
                                            value: "Collaborations"
                                        },
                                        {
                                            value: "Hosts"
                                        },
                                        {
                                            value: "Vendors"
                                        }, 
                                    ].map((option, index)=>/*#__PURE__*/ jsx_runtime_.jsx(external_antd_.Menu.Item, {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                onClick: (e)=>e.preventDefault(),
                                                children: option.value || ""
                                            })
                                        }, index))
                                }),
                                placement: "bottomRight",
                                trigger: [
                                    "hover"
                                ],
                                arrow: true,
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                    onClick: (e)=>e.preventDefault(),
                                    children: [
                                        `Collaborations `,
                                        /*#__PURE__*/ jsx_runtime_.jsx(icons_.DownOutlined, {})
                                    ]
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (drawer_coll_module_default()).shareAshelfDiv,
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                            className: (drawer_coll_module_default()).shareAshelfTxtSpan,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: (drawer_coll_module_default()).shareSpan,
                                    children: "share"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: (drawer_coll_module_default()).aSpan,
                                    children: "A"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: (drawer_coll_module_default()).shelfSpan,
                                    children: "shelf"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (drawer_coll_module_default()).menuPackDiv,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (drawer_coll_module_default()).rectangleDiv
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (drawer_coll_module_default()).floatingLabelDiv,
                                children: /*#__PURE__*/ jsx_runtime_.jsx("b", {
                                    className: (drawer_coll_module_default()).label,
                                    children: "Filters"
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP,
                                children: "Food"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP1,
                                children: "Drinks"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck1,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP2,
                                children: "Clothing"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck2,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP3,
                                children: "Cosmetics"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP4,
                                children: "Art and Crafts"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck3,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck4,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP5,
                                children: "Souvenirs"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck5,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                className: (drawer_coll_module_default()).productCategoriesH6,
                                children: "Product Categories:"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                className: (drawer_coll_module_default()).collaborationDatesH6,
                                children: "Collaboration Dates:"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                className: (drawer_coll_module_default()).augustStrong,
                                children: "12-19 August"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (drawer_coll_module_default()).lineDiv
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP6,
                                children: "Cafe"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck6,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP7,
                                children: "Restaurant"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck7,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP8,
                                children: "Venue"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck8,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP9,
                                children: "Community"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP10,
                                children: "Studios"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck9,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck10,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (drawer_coll_module_default()).trailingDataP11,
                                children: "Co-Work"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_bootstrap_.Form.Check, {
                                className: (drawer_coll_module_default()).trailingIconFormCheck11,
                                isInvalid: true
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                className: (drawer_coll_module_default()).hostCategoriesH6,
                                children: "Host Categories:"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (drawer_coll_module_default()).lineDiv1
                            })
                        ]
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const drawer_coll = (DrawerColl);

// EXTERNAL MODULE: ./components/portal-drawer.js
var portal_drawer = __webpack_require__(4859);
// EXTERNAL MODULE: ../node_modules/next/link.js
var next_link = __webpack_require__(9894);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: ./components/menu-drawer.js
var menu_drawer = __webpack_require__(4627);
// EXTERNAL MODULE: ./pages/collaborations.module.css
var collaborations_module = __webpack_require__(4602);
var collaborations_module_default = /*#__PURE__*/__webpack_require__.n(collaborations_module);
;// CONCATENATED MODULE: ./pages/collaborations.js









const Collaborations = ()=>{
    const { 0: isCollaborationsPopupOpen , 1: setCollaborationsPopupOpen  } = (0,external_react_.useState)(false);
    const { 0: isDrawerCollOpen , 1: setDrawerCollOpen  } = (0,external_react_.useState)(false);
    const { 0: isDrawerColl1Open , 1: setDrawerColl1Open  } = (0,external_react_.useState)(false);
    const { 0: isMenuDrawerOpen , 1: setMenuDrawerOpen  } = (0,external_react_.useState)(false);
    const { 0: isCollaborationsPopup1Open , 1: setCollaborationsPopup1Open  } = (0,external_react_.useState)(false);
    const openCollaborationsPopup = (0,external_react_.useCallback)(()=>{
        setCollaborationsPopupOpen(true);
    }, []);
    const closeCollaborationsPopup = (0,external_react_.useCallback)(()=>{
        setCollaborationsPopupOpen(false);
    }, []);
    const openDrawerColl = (0,external_react_.useCallback)(()=>{
        setDrawerCollOpen(true);
    }, []);
    const closeDrawerColl = (0,external_react_.useCallback)(()=>{
        setDrawerCollOpen(false);
    }, []);
    const openDrawerColl1 = (0,external_react_.useCallback)(()=>{
        setDrawerColl1Open(true);
    }, []);
    const closeDrawerColl1 = (0,external_react_.useCallback)(()=>{
        setDrawerColl1Open(false);
    }, []);
    const openMenuDrawer = (0,external_react_.useCallback)(()=>{
        setMenuDrawerOpen(true);
    }, []);
    const closeMenuDrawer = (0,external_react_.useCallback)(()=>{
        setMenuDrawerOpen(false);
    }, []);
    const openCollaborationsPopup1 = (0,external_react_.useCallback)(()=>{
        setCollaborationsPopup1Open(true);
    }, []);
    const closeCollaborationsPopup1 = (0,external_react_.useCallback)(()=>{
        setCollaborationsPopup1Open(false);
    }, []);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (collaborations_module_default()).collaborationsDiv,
                onClick: openCollaborationsPopup1,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("iframe", {
                        className: (collaborations_module_default()).rectangleIframe,
                        src: `https://maps.google.com/maps?q=charring%20cross&t=&z=15&ie=UTF8&iwloc=&output=embed`,
                        onClick: openCollaborationsPopup
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                        className: (collaborations_module_default()).locationOnIcon,
                        alt: "",
                        src: "../location-on.svg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                        className: (collaborations_module_default()).iconButton,
                        onClick: openDrawerColl,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                            className: (collaborations_module_default()).arrowForwardIosIcon,
                            alt: "",
                            src: "../arrow-forward-ios.svg"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                        className: (collaborations_module_default()).zoomOutIcon,
                        alt: "",
                        src: "../zoom-out.svg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                        className: (collaborations_module_default()).zoomInIcon,
                        alt: "",
                        src: "../zoom-in.svg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                        className: (collaborations_module_default()).mapViewButton,
                        onClick: openDrawerColl1,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (collaborations_module_default()).labelTextDiv,
                            children: "Map View: Collaborations"
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (collaborations_module_default()).headerMainsiteDiv,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: "/",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: (collaborations_module_default()).shareAshelf,
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                        className: (collaborations_module_default()).shareAshelfTxtSpan,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: (collaborations_module_default()).shareSpan,
                                                children: "share"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: (collaborations_module_default()).aSpan,
                                                children: "A"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: (collaborations_module_default()).shelfSpan,
                                                children: "shelf"
                                            })
                                        ]
                                    })
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: (collaborations_module_default()).groupButton,
                                onClick: openMenuDrawer,
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (collaborations_module_default()).groupDiv,
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: (collaborations_module_default()).inputChipDiv,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    className: (collaborations_module_default()).userImagesUserImages,
                                                    alt: "",
                                                    src: "../user-imagesuser-images.svg"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: (collaborations_module_default()).labelTextDiv1,
                                                    children: "Enabled"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                            className: (collaborations_module_default()).menuIcon,
                                            alt: ""
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                            className: (collaborations_module_default()).menuIcon1,
                                            alt: "",
                                            src: "../menu.svg"
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                className: (collaborations_module_default()).whatIsShareAshelf,
                                children: "what is shareAshelf?"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: "/signup",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: (collaborations_module_default()).signUpA,
                                    children: "Sign Up:"
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (collaborations_module_default()).formFooterDiv,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).rectangleDiv
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).rentmyshelfIncDiv,
                                children: "\xa9 2022 rentmyshelf, Inc."
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).supportDiv,
                                children: "Support"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).contactDiv,
                                children: "Contact"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).termsOfService,
                                children: "Terms of Service"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (collaborations_module_default()).lineDiv
                            })
                        ]
                    })
                ]
            }),
            isCollaborationsPopupOpen && /*#__PURE__*/ jsx_runtime_.jsx(portal_popup/* default */.Z, {
                overlayColor: "rgba(113, 113, 113, 0.3)",
                placement: "Centered",
                onOutsideClick: closeCollaborationsPopup,
                children: /*#__PURE__*/ jsx_runtime_.jsx(collaborations_popup1, {
                    onClose: closeCollaborationsPopup
                })
            }),
            isDrawerCollOpen && /*#__PURE__*/ jsx_runtime_.jsx(portal_drawer/* default */.Z, {
                overlayColor: "rgba(113, 113, 113, 0.3)",
                placement: "Left",
                onOutsideClick: closeDrawerColl,
                children: /*#__PURE__*/ jsx_runtime_.jsx(drawer_coll, {
                    onClose: closeDrawerColl
                })
            }),
            isDrawerColl1Open && /*#__PURE__*/ jsx_runtime_.jsx(portal_drawer/* default */.Z, {
                overlayColor: "rgba(113, 113, 113, 0.3)",
                placement: "Left",
                onOutsideClick: closeDrawerColl1,
                children: /*#__PURE__*/ jsx_runtime_.jsx(drawer_coll, {
                    onClose: closeDrawerColl1
                })
            }),
            isMenuDrawerOpen && /*#__PURE__*/ jsx_runtime_.jsx(portal_drawer/* default */.Z, {
                overlayColor: "rgba(113, 113, 113, 0.3)",
                placement: "Right",
                onOutsideClick: closeMenuDrawer,
                children: /*#__PURE__*/ jsx_runtime_.jsx(menu_drawer/* default */.Z, {
                    onClose: closeMenuDrawer
                })
            }),
            isCollaborationsPopup1Open && /*#__PURE__*/ jsx_runtime_.jsx(portal_popup/* default */.Z, {
                overlayColor: "rgba(113, 113, 113, 0.3)",
                placement: "Centered",
                onOutsideClick: closeCollaborationsPopup1,
                children: /*#__PURE__*/ jsx_runtime_.jsx(collaborations_popup1, {
                    onClose: closeCollaborationsPopup1
                })
            })
        ]
    });
};
/* harmony default export */ const collaborations = (Collaborations);


/***/ }),

/***/ 6642:
/***/ (() => {



/***/ })

};
;