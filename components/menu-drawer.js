import { useEffect } from "react";
import Link from "next/link";
import styles from "./menu-drawer.module.css";

const MenuDrawer = ({ onClose }) => {
  useEffect(() => {
    const scrollAnimElements = document.querySelectorAll(
      "[data-animate-on-scroll]"
    );
    const observer = new IntersectionObserver(
      (entries) => {
        for (const entry of entries) {
          if (entry.isIntersecting || entry.intersectionRatio > 0) {
            const targetElement = entry.target;
            targetElement.classList.add(styles.animate);
            observer.unobserve(targetElement);
          }
        }
      },
      {
        threshold: 0.15,
      }
    );

    for (let i = 0; i < scrollAnimElements.length; i++) {
      observer.observe(scrollAnimElements[i]);
    }

    return () => {
      for (let i = 0; i < scrollAnimElements.length; i++) {
        observer.unobserve(scrollAnimElements[i]);
      }
    };
  }, []);

  return (
    <div className={styles.menuDrawerDiv} data-animate-on-scroll>
      <div className={styles.frameDiv}>
        <div className={styles.frameDiv}>
          <div className={styles.rectangleDiv} />
          <Link href="/signup">
            <a className={styles.signupA}>Signup</a>
          </Link>
          <Link href="/collaborations">
            <a className={styles.main}>Main</a>
          </Link>
          <Link href="/hosts">
            <a className={styles.hostsA}>Hosts</a>
          </Link>
        </div>
      </div>
      <Link href="/">
        <a className={styles.homeA}>Home</a>
      </Link>
    </div>
  );
};

export default MenuDrawer;
