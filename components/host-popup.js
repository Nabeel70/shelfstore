import { useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from "react-bootstrap";
import styles from "./host-popup.module.css";

const HostPopup = ({ onClose }) => {
  const onButtonClick = useCallback(() => {
    window.location.href = "#";
  }, []);

  return (
    <div className={styles.hostPopupDiv}>
      <div className={styles.headerDiv}>
        <div className={styles.contentDiv}>
          <div className={styles.textDiv}>
            <h6 className={styles.headerH6}>Checkmate</h6>
            <p className={styles.subheadP}>Gaming Cafe</p>
          </div>
        </div>
        <Button
          className={styles.closeButton}
          variant="outline-secondary"
          href="/hosts"
        >
          ✕
        </Button>
      </div>
      <img className={styles.mediaIcon} alt="" src="../media@2x.png" />
      <div className={styles.supportingTextDiv}>
        <p className={styles.supportingText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
      </div>
      <button className={styles.button} autoFocus onClick={onButtonClick}>
        <div className={styles.labelTextDiv}>See Profile</div>
      </button>
      <div className={styles.buttonDiv}>
        <div className={styles.labelTextDiv1}>Enabled</div>
      </div>
    </div>
  );
};

export default HostPopup;
