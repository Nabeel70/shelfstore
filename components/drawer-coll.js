import { useEffect } from "react";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Menu, Dropdown, Button } from "antd";
import { Form } from "react-bootstrap";
import {
  DownOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
  CalendarOutlined,
  CheckOutlined,
  ClockCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  HeartOutlined,
  LeftOutlined,
  LockOutlined,
  MailOutlined,
  PaperClipOutlined,
  PhoneOutlined,
  QuestionCircleOutlined,
  ReloadOutlined,
  RightOutlined,
  SearchOutlined,
  SendOutlined,
  ShareAltOutlined,
  UserOutlined,
} from "@ant-design/icons";
import styles from "./drawer-coll.module.css";

const DrawerColl = ({ onClose }) => {
  useEffect(() => {
    const scrollAnimElements = document.querySelectorAll(
      "[data-animate-on-scroll]"
    );
    const observer = new IntersectionObserver(
      (entries) => {
        for (const entry of entries) {
          if (entry.isIntersecting || entry.intersectionRatio > 0) {
            const targetElement = entry.target;
            targetElement.classList.add(styles.animate);
            observer.unobserve(targetElement);
          }
        }
      },
      {
        threshold: 0.15,
      }
    );

    for (let i = 0; i < scrollAnimElements.length; i++) {
      observer.observe(scrollAnimElements[i]);
    }

    return () => {
      for (let i = 0; i < scrollAnimElements.length; i++) {
        observer.unobserve(scrollAnimElements[i]);
      }
    };
  }, []);

  return (
    <div className={styles.drawerCollDiv} data-animate-on-scroll>
      <div className={styles.supportDiv}>Support</div>
      <div className={styles.contactDiv}>Contact</div>
      <div className={styles.termsOfService}>Terms of Service</div>
      <div className={styles.shareashelfLtdDiv}>© 2022 Shareashelf Ltd.</div>
      <div className={styles.frameDiv}>
        <div className={styles.mobileMenuDiv}>
          <div className={styles.fieldDiv}>
            <Dropdown
              className={styles.frameDropdown}
              overlay={
                <Menu>
                  {[
                    { value: "Collaborations" },
                    { value: "Hosts" },
                    { value: "Vendors" },
                  ].map((option, index) => (
                    <Menu.Item key={index}>
                      <a onClick={(e) => e.preventDefault()}>
                        {option.value || ""}
                      </a>
                    </Menu.Item>
                  ))}
                </Menu>
              }
              placement="bottomRight"
              trigger={["hover"]}
              arrow={true}
            >
              <a onClick={(e) => e.preventDefault()}>
                {`Collaborations `}
                <DownOutlined />
              </a>
            </Dropdown>
          </div>
        </div>
        <div className={styles.shareAshelfDiv}>
          <span className={styles.shareAshelfTxtSpan}>
            <span className={styles.shareSpan}>share</span>
            <span className={styles.aSpan}>A</span>
            <span className={styles.shelfSpan}>shelf</span>
          </span>
        </div>
        <div className={styles.menuPackDiv}>
          <div className={styles.rectangleDiv} />
          <div className={styles.floatingLabelDiv}>
            <b className={styles.label}>Filters</b>
          </div>
          <p className={styles.trailingDataP}>Food</p>
          <Form.Check className={styles.trailingIconFormCheck} isInvalid />
          <p className={styles.trailingDataP1}>Drinks</p>
          <Form.Check className={styles.trailingIconFormCheck1} isInvalid />
          <p className={styles.trailingDataP2}>Clothing</p>
          <Form.Check className={styles.trailingIconFormCheck2} isInvalid />
          <p className={styles.trailingDataP3}>Cosmetics</p>
          <p className={styles.trailingDataP4}>Art and Crafts</p>
          <Form.Check className={styles.trailingIconFormCheck3} isInvalid />
          <Form.Check className={styles.trailingIconFormCheck4} isInvalid />
          <p className={styles.trailingDataP5}>Souvenirs</p>
          <Form.Check className={styles.trailingIconFormCheck5} isInvalid />
          <h6 className={styles.productCategoriesH6}>Product Categories:</h6>
          <h6 className={styles.collaborationDatesH6}>Collaboration Dates:</h6>
          <strong className={styles.augustStrong}>12-19 August</strong>
          <div className={styles.lineDiv} />
          <p className={styles.trailingDataP6}>Cafe</p>
          <Form.Check className={styles.trailingIconFormCheck6} isInvalid />
          <p className={styles.trailingDataP7}>Restaurant</p>
          <Form.Check className={styles.trailingIconFormCheck7} isInvalid />
          <p className={styles.trailingDataP8}>Venue</p>
          <Form.Check className={styles.trailingIconFormCheck8} isInvalid />
          <p className={styles.trailingDataP9}>Community</p>
          <p className={styles.trailingDataP10}>Studios</p>
          <Form.Check className={styles.trailingIconFormCheck9} isInvalid />
          <Form.Check className={styles.trailingIconFormCheck10} isInvalid />
          <p className={styles.trailingDataP11}>Co-Work</p>
          <Form.Check className={styles.trailingIconFormCheck11} isInvalid />
          <h6 className={styles.hostCategoriesH6}>Host Categories:</h6>
          <div className={styles.lineDiv1} />
        </div>
      </div>
    </div>
  );
};

export default DrawerColl;
