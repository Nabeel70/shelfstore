import { useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from "react-bootstrap";
import styles from "./collaborations-popup1.module.css";

const CollaborationsPopup1 = ({ onClose }) => {
  const onButtonClick = useCallback(() => {
    window.location.href = "#";
  }, []);

  const onButton1Click = useCallback(() => {
    window.location.href = "#";
  }, []);

  return (
    <div className={styles.collaborationsPopupDiv}>
      <div className={styles.headerDiv}>
        <div className={styles.contentDiv}>
          <div className={styles.textDiv}>
            <h6 className={styles.headerH6}>Checkmate</h6>
            <p className={styles.subheadP}>Gaming Cafe</p>
          </div>
        </div>
        <Button
          className={styles.closeButton}
          variant="outline-secondary"
          href="/collaborations"
        >
          ✕
        </Button>
      </div>
      <img className={styles.mediaIcon} alt="" src="../media@2x.png" />
      <div className={styles.supportingTextDiv}>
        <p className={styles.supportingText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
      </div>
      <div className={styles.buttonDiv}>
        <div className={styles.labelTextDiv}>Enabled</div>
      </div>
      <button className={styles.button} autoFocus onClick={onButtonClick}>
        <div className={styles.labelTextDiv1}>See Profile</div>
      </button>
      <div className={styles.headerDiv1}>
        <div className={styles.contentDiv}>
          <div className={styles.textDiv}>
            <h6 className={styles.headerH61}>AlphaCostumes</h6>
            <p className={styles.subheadP}>
              Professional Cosplay Costume Maker
            </p>
          </div>
        </div>
      </div>
      <img className={styles.mediaIcon1} alt="" src="../media1@2x.png" />
      <div className={styles.supportingTextDiv1}>
        <p className={styles.supportingText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
      </div>
      <div className={styles.buttonDiv1}>
        <div className={styles.labelTextDiv}>Enabled</div>
      </div>
      <button className={styles.button1} autoFocus onClick={onButton1Click}>
        <div className={styles.labelTextDiv1}>See Profile</div>
      </button>
      <b className={styles.b}>🤝</b>
      <p className={styles.augustP}>12-19 August</p>
    </div>
  );
};

export default CollaborationsPopup1;
